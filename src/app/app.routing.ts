import { Routes, RouterModule } from '@angular/router';

import { JobItemListComponent } from './job-item-list/job-item-list.component';
import { JobItemComponent } from './job-item/job-item.component';
import { JobItemFormComponent } from './job-item-form/job-item-form.component';

const appRoutes: Routes = [
    { path: 'list', component: JobItemListComponent },
    { path: 'item/:id',component: JobItemComponent,},
    { path: 'postajob', component: JobItemFormComponent},
    { path: '', pathMatch: 'full', redirectTo: 'list' }
];

export const Routing = RouterModule.forRoot(appRoutes);
