import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-job-item-list',
  templateUrl: './job-item-list.component.html',
  styleUrls: ['./job-item-list.component.css']
})
export class JobItemListComponent implements OnInit {
  jobItemsRef: AngularFireList<any>;
  jobItems: Observable<any[]>;

  constructor(private afDb: AngularFireDatabase) {
    this.jobItemsRef = afDb.list('/jobs');
    // Store keys
    this.jobItems = this.jobItemsRef.snapshotChanges().pipe(
      map(changes =>
          changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  ngOnInit() {

  }
}
