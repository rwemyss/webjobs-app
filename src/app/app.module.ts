import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from 'angularfire2'
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { Routing } from './app.routing';

import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { JobItemComponent } from './job-item/job-item.component';
import { JobItemListComponent } from './job-item-list/job-item-list.component';
import { JobItemSummaryComponent } from './job-item-summary/job-item-summary.component';
import { JobItemFormComponent } from './job-item-form/job-item-form.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';

export const firebaseConfig = {
  apiKey: 'AIzaSyDZUF_E28NnkgPoquW89P1giuj3mvBD2Bg',
  authDomain: 'webjobs-9f6af.firebaseapp.com',
  databaseURL: 'https://webjobs-9f6af.firebaseio.com',
  storageBucket: 'webjobs-9f6af.appspot.com',
  messagingSenderId: '464965313777',
  projectId: 'webjobs-9f6af'
};

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    JobItemComponent,
    JobItemListComponent,
    JobItemSummaryComponent,
    JobItemFormComponent,
    NavBarComponent,
    BottomBarComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    Routing,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
