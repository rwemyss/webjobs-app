import { Component, OnInit, Input} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-job-item-summary',
  templateUrl: './job-item-summary.component.html',
  styleUrls: ['./job-item-summary.component.css']
})
export class JobItemSummaryComponent implements OnInit {
  @Input() jobItem;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  loadDetails(jobItem) {
    console.log("Loading job...");
    this.router.navigate(['/item', jobItem.key]);
  }

}
