import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';

import { CustomValidators } from 'ng2-validation';

@Component({
  selector: 'app-job-item-form',
  templateUrl: './job-item-form.component.html',
  styleUrls: ['./job-item-form.component.css']
})
export class JobItemFormComponent implements OnInit {
  form: FormGroup;
  jobItems: AngularFireList<any[]>;

  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private afDb: AngularFireDatabase
  ) {
    this.jobItems = afDb.list('/jobs');
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      // Contact details
      email: this.formBuilder.control('', Validators.compose([
        Validators.required,
        CustomValidators.email
      ])),

      // Job Details
      title: this.formBuilder.control('', Validators.required),
      location: this.formBuilder.control('', Validators.required),
      jobType: this.formBuilder.control('', Validators.required),
      tags: this.formBuilder.control(''),
      description: this.formBuilder.control('', Validators.required),
      //TODO If you put in a min then you need a max and vice versa
      salaryMin: this.formBuilder.control(''),
      salaryMax: this.formBuilder.control(''),
      applicationDetails: this.formBuilder.control('', Validators.required),

      //Company Details
      companyName: this.formBuilder.control('', Validators.required),
      companyDescription: this.formBuilder.control(''),
      companyWebsite: this.formBuilder.control(''),
      companyTwitter: this.formBuilder.control('')
    });
  }

  onSubmit(jobItem) {
    let ref = this.jobItems.push(jobItem);
    this.form.reset();
    this.router.navigate(['/list']);
  }
}
