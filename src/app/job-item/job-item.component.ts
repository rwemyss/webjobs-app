import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from "rxjs/index";

@Component({
  selector: 'app-job-item',
  templateUrl: './job-item.component.html',
  styleUrls: ['./job-item.component.css']
})
export class JobItemComponent implements OnInit {
  jobItem: Observable<any[]>;
  ref;

  constructor(private activatedRoute: ActivatedRoute,
              private afDb: AngularFireDatabase) {
    this.ref = afDb;
  }

  ngOnInit() {
    this.activatedRoute.params
      .subscribe(params => {
        this.getJobItemDetails(params['id']);
      });
  }

  getJobItemDetails(key: string) {
    this.jobItem = this.ref.object(`/jobs/${key}`).valueChanges();
  }
}
