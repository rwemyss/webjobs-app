# WebJobs Side Project

This project was create to explore using [Firebase](https://firebase.google.com/) (BaaS) and Angular 2+. 
It's a simple job posting site demonstrating CRUD operations that any web application would use. It uses Websockets for
realtime transfer of data.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change 
any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag 
for a production build.

## Todo
* Implement search
* Complete post job form
* Add post job approval process
* Connect to job listing APIs to populate app with data
* Add testing
* Tidy UI features like loading spinner etc
